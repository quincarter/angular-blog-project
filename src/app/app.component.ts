import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subscription } from 'rxjs';
import { User } from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'angular-blog-site';

  user: User;

  private userSubscription: Subscription;

  constructor(
    private fireAuth: AngularFireAuth
  ) {
  }

  ngOnInit(): void {
    this.userSubscription = this.fireAuth.user.subscribe(data => this.user = data);
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

}
