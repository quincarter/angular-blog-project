// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAyNkMFc_ysW1FdpX-gm_IemZRCoVj4p8I',
    authDomain: 'quincarter-blog.firebaseapp.com',
    databaseURL: 'https://quincarter-blog.firebaseio.com',
    projectId: 'quincarter-blog',
    storageBucket: 'quincarter-blog.appspot.com',
    messagingSenderId: '493530617304',
    appId: '1:493530617304:web:ed4dbc181be0f6cd'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
